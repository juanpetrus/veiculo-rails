class VeiculoController < ApplicationController
  before_action :set_veiculo, only: [:consultar]

  def consultar
    if @veiculo.present?
      render json: @veiculo
    else
      render json: {status: "erro", mensagem: "Contador não encontrado, por favor consulte a documentação"}
    end
  end

  def create
    @veiculo = Veiculo.new(veiculo_params)
    if @veiculo.save
      render json: @veiculo, status: :created
    else
      render json: @veiculo.errors
    end
  end

  private
  def set_veiculo
    unless @veiculo = Veiculo.find_by(placa: params[:placa])
      render json: {status: "erro", mensagem: "veiculo não encontrado, por favor consulte a documentação"}
    end
  end

  def veiculo_params
    params.require(:veiculo).permit(:marca, :modelo, :placa)
  end
end
