Rails.application.routes.draw do
  # METHODOS
  post '/veiculo/cadastro', to: 'veiculo#create'
  get '/veiculo/consulta/:placa', to: 'veiculo#consultar'
end
